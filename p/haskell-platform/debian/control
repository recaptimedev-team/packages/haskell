Source: haskell-platform
Section: haskell
Priority: optional
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Joachim Breitner <nomeata@debian.org>
Standards-Version: 4.1.4
Rules-Requires-Root: no
Build-Depends: cdbs, debhelper (>= 10)
Homepage: http://hackage.haskell.org/platform/
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-platform]

Package: haskell-platform
Architecture: all
Depends:
  ghc (>= 8.4.3),
  libghc-async-dev,
  libghc-attoparsec-dev,
  libghc-case-insensitive-dev,
  libghc-fgl-dev,
  libghc-gluraw-dev,
  libghc-glut-dev,
  libghc-hashable-dev,
  libghc-haskell-src-dev,
  libghc-html-dev,
  libghc-http-dev,
  libghc-hunit-dev,
  libghc-network-dev,
  libghc-opengl-dev,
  libghc-openglraw-dev,
  libghc-parallel-dev,
  libghc-primitive-dev,
  libghc-quickcheck2-dev,
  libghc-random-dev,
  libghc-regex-base-dev,
  libghc-regex-compat-dev,
  libghc-regex-posix-dev,
  libghc-split-dev,
  libghc-syb-dev,
  libghc-transformers-dev,
  libghc-unordered-containers-dev,
  libghc-vector-dev,
  libghc-xhtml-dev,
  libghc-zlib-dev,
  alex,
  cabal-install,
  ghc-haddock,
  happy,
  hscolour,
  ${misc:Depends}
Suggests:
  haskell-platform-doc,
  haskell-platform-prof
Description: Standard Haskell libraries and tools
 The Haskell Platform is a suite of tools and libraries that contain the most
 important and best supported components. It is meant to be a starting point
 for Haskell developers who are looking for libraries to use.
 .
 Installing this meta package will also install the Debian packages containing
 the libraries and tools as specified in the official Haskell Platform, in the
 version available in Debian. This may or may not be the version specified in
 the platform. If this is of relevance to you, please check them on the
 official Haskell Platform homepage.

Package: haskell-platform-prof
Section: haskell
Architecture: all
Description: Standard Haskell libraries and tools; profiling libraries
 The Haskell Platform is a suite of tools and libraries that contain the most
 important and best supported components. It is meant to be a starting point
 for Haskell developers who are looking for libraries to use.
 .
 Installing this meta package will install the profiling data for the
 libraries as specified in the official Haskell Platform.
Depends: haskell-platform,
  ghc-prof,
  libghc-async-prof,
  libghc-attoparsec-prof,
  libghc-case-insensitive-prof,
  libghc-fgl-prof,
  libghc-gluraw-prof,
  libghc-glut-prof,
  libghc-hashable-prof,
  libghc-haskell-src-prof,
  libghc-html-prof,
  libghc-http-prof,
  libghc-hunit-prof,
  libghc-network-prof,
  libghc-opengl-prof,
  libghc-openglraw-prof,
  libghc-parallel-prof,
  libghc-primitive-prof,
  libghc-quickcheck2-prof,
  libghc-random-prof,
  libghc-regex-base-prof,
  libghc-regex-compat-prof,
  libghc-regex-posix-prof,
  libghc-split-prof,
  libghc-syb-prof,
  libghc-transformers-prof,
  libghc-unordered-containers-prof,
  libghc-vector-prof,
  libghc-xhtml-prof,
  libghc-zlib-prof,
  ${misc:Depends}

Package: haskell-platform-doc
Section: doc
Architecture: all
Recommends: haskell-platform
Description: Standard Haskell libraries and tools; documentation
 The Haskell Platform is a suite of tools and libraries that contain the most
 important and best supported components. It is meant to be a starting point
 for Haskell developers who are looking for libraries to use.
 .
 Installing this meta package will install the documentation for the
 libraries as specified in the official Haskell Platform.
Depends:
  ghc-doc,
  libghc-async-doc,
  libghc-attoparsec-doc,
  libghc-case-insensitive-doc,
  libghc-fgl-doc,
  libghc-gluraw-doc,
  libghc-glut-doc,
  libghc-hashable-doc,
  libghc-haskell-src-doc,
  libghc-html-doc,
  libghc-http-doc,
  libghc-hunit-doc,
  libghc-network-doc,
  libghc-opengl-doc,
  libghc-openglraw-doc,
  libghc-parallel-doc,
  libghc-primitive-doc,
  libghc-quickcheck2-doc,
  libghc-random-doc,
  libghc-regex-base-doc,
  libghc-regex-compat-doc,
  libghc-regex-posix-doc,
  libghc-split-doc,
  libghc-syb-doc,
  libghc-transformers-doc,
  libghc-unordered-containers-doc,
  libghc-vector-doc,
  libghc-xhtml-doc,
  libghc-zlib-doc,
  ${misc:Depends}
