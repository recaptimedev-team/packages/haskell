Source: haskell-lens
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-base-orphans-dev (>= 0.5.2),
 libghc-base-orphans-dev (<< 1),
 libghc-base-orphans-dev (>= 0.3),
 libghc-base-orphans-prof,
 libghc-bifunctors-dev (>= 5.1),
 libghc-bifunctors-dev (<< 6),
 libghc-bifunctors-dev (>= 5),
 libghc-bifunctors-prof,
 libghc-call-stack-dev (>= 0.1),
 libghc-call-stack-dev (<< 0.3),
 libghc-call-stack-prof,
 libghc-comonad-dev (>= 4),
 libghc-comonad-dev (<< 6),
 libghc-comonad-dev (>= 4),
 libghc-comonad-prof,
 libghc-contravariant-dev (<< 2),
 libghc-contravariant-dev (>= 1.3),
 libghc-contravariant-prof,
 libghc-distributive-dev (<< 1),
 libghc-distributive-dev (>= 0.3),
 libghc-distributive-prof,
 libghc-exceptions-dev (<< 1),
 libghc-exceptions-dev (>= 0.1.1),
 libghc-exceptions-prof,
 libghc-free-dev (>= 4),
 libghc-free-dev (<< 6),
 libghc-free-prof,
 libghc-hashable-dev (>= 1.1.2.3),
 libghc-hashable-dev (<< 1.4),
 libghc-hashable-prof,
 libghc-hunit-dev (>= 1.2),
 libghc-kan-extensions-dev (<< 6),
 libghc-kan-extensions-dev (>= 5),
 libghc-kan-extensions-prof,
 libghc-parallel-dev (<< 3.3),
 libghc-parallel-dev (>= 3.1.0.1),
 libghc-parallel-prof,
 libghc-profunctors-dev (>= 5.2.1),
 libghc-profunctors-dev (<< 6),
 libghc-profunctors-dev (>= 5),
 libghc-profunctors-prof,
 libghc-quickcheck2-dev (>= 2.4),
 libghc-reflection-dev (<< 3),
 libghc-reflection-dev (>= 2.1),
 libghc-reflection-prof,
 libghc-semigroupoids-dev (<< 6),
 libghc-semigroupoids-dev (>= 5),
 libghc-semigroupoids-prof,
 libghc-tagged-dev (>= 0.4.4),
 libghc-tagged-dev (<< 1),
 libghc-tagged-dev (>= 0.4.4),
 libghc-tagged-prof,
 libghc-th-abstraction-dev (>= 0.3),
 libghc-th-abstraction-dev (<< 0.4),
 libghc-th-abstraction-prof,
 libghc-transformers-compat-dev (>= 0.4),
 libghc-transformers-compat-dev (<< 1),
 libghc-transformers-compat-dev (>= 0.4),
 libghc-transformers-compat-prof,
 libghc-type-equality-dev (>= 1),
 libghc-type-equality-dev (<< 2),
 libghc-type-equality-prof,
 libghc-unordered-containers-dev (>= 0.2.4),
 libghc-unordered-containers-dev (<< 0.3),
 libghc-unordered-containers-prof,
 libghc-vector-dev (>= 0.9),
 libghc-vector-dev (<< 0.13),
 libghc-vector-prof,
 libghc-hunit-dev (>= 1.2),
 libghc-hunit-prof,
 libghc-quickcheck2-dev (>= 2.4),
 libghc-quickcheck2-prof,
 libghc-test-framework-dev (>= 0.6),
 libghc-test-framework-prof,
 libghc-test-framework-hunit-dev (>= 0.2),
 libghc-test-framework-hunit-prof,
 libghc-test-framework-quickcheck2-dev (>= 0.2),
 libghc-test-framework-quickcheck2-prof,
 libghc-test-framework-th-dev (>= 0.2) [arm64 armel armhf powerpc ppc64 ppc64el s390x sparc any-amd64 any-i386],
 libghc-test-framework-th-prof,
Build-Depends-Indep: ghc-doc,
 libghc-base-orphans-doc,
 libghc-bifunctors-doc,
 libghc-call-stack-doc,
 libghc-comonad-doc,
 libghc-contravariant-doc,
 libghc-distributive-doc,
 libghc-exceptions-doc,
 libghc-free-doc,
 libghc-hashable-doc,
 libghc-kan-extensions-doc,
 libghc-parallel-doc,
 libghc-profunctors-doc,
 libghc-reflection-doc,
 libghc-semigroupoids-doc,
 libghc-tagged-doc,
 libghc-th-abstraction-doc,
 libghc-transformers-compat-doc,
 libghc-type-equality-doc,
 libghc-unordered-containers-doc,
 libghc-vector-doc,
Standards-Version: 4.5.0
Homepage: https://github.com/ekmett/lens/
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-lens
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-lens]

Package: libghc-lens-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Lenses, Folds and Traversals
 This package comes "Batteries Included" with many useful lenses for the types
 commonly used from the Haskell Platform, and with tools for automatically
 generating lenses and isomorphisms for user-supplied data types.
 .
 The combinators in Control.Lens provide a highly generic toolbox for composing
 families of getters, folds, isomorphisms, traversals, setters and lenses and
 their indexed variants.
 .
 More information on the care and feeding of lenses, including a tutorial and
 motivation for their types can be found on the lens wiki
 (https://github.com/ekmett/lens/wiki).
 .
 This package contains the normal library files.

Package: libghc-lens-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Lenses, Folds and Traversals; profiling libraries
 This package comes "Batteries Included" with many useful lenses for the types
 commonly used from the Haskell Platform, and with tools for automatically
 generating lenses and isomorphisms for user-supplied data types.
 .
 The combinators in Control.Lens provide a highly generic toolbox for composing
 families of getters, folds, isomorphisms, traversals, setters and lenses and
 their indexed variants.
 .
 More information on the care and feeding of lenses, including a tutorial and
 motivation for their types can be found on the lens wiki
 (https://github.com/ekmett/lens/wiki).
 .
 This package contains the libraries compiled with profiling enabled.

Package: libghc-lens-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Lenses, Folds and Traversals; documentation
 This package comes "Batteries Included" with many useful lenses for the types
 commonly used from the Haskell Platform, and with tools for automatically
 generating lenses and isomorphisms for user-supplied data types.
 .
 The combinators in Control.Lens provide a highly generic toolbox for composing
 families of getters, folds, isomorphisms, traversals, setters and lenses and
 their indexed variants.
 .
 More information on the care and feeding of lenses, including a tutorial and
 motivation for their types can be found on the lens wiki
 (https://github.com/ekmett/lens/wiki).
 .
 This package contains the documentation files.
