Source: haskell-hedgehog
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends: debhelper (>= 10),
 haskell-devscripts (>= 0.8),
 cdbs,
 ghc (>= 8.4.3),
 ghc-prof,
 libghc-ansi-terminal-dev (>= 0.6),
 libghc-ansi-terminal-dev (<< 0.11),
 libghc-ansi-terminal-prof,
 libghc-async-dev (>= 2.0),
 libghc-async-dev (<< 2.3),
 libghc-async-prof (>= 2.0),
 libghc-async-prof (<< 2.3),
 libghc-concurrent-output-dev (>= 1.7),
 libghc-concurrent-output-dev (<< 1.11),
 libghc-concurrent-output-prof,
 libghc-erf-dev (>= 2.0),
 libghc-erf-dev (<< 2.1),
 libghc-erf-prof,
 libghc-exceptions-dev (>= 0.7),
 libghc-exceptions-dev (<< 0.11),
 libghc-exceptions-prof,
 libghc-lifted-async-dev (>= 0.7),
 libghc-lifted-async-dev (<< 0.11),
 libghc-lifted-async-prof (>= 0.7),
 libghc-lifted-async-prof (<< 0.11),
 libghc-mmorph-dev (>= 1.0),
 libghc-mmorph-dev (<< 1.2),
 libghc-mmorph-prof (>= 1.0),
 libghc-mmorph-prof (<< 1.2),
 libghc-monad-control-dev (>= 1.0),
 libghc-monad-control-dev (<< 1.1),
 libghc-monad-control-prof (>= 1.0),
 libghc-monad-control-prof (<< 1.1),
 libghc-pretty-show-dev (>= 1.6),
 libghc-pretty-show-dev (<< 1.11),
 libghc-pretty-show-prof,
 libghc-primitive-dev (>= 0.6),
 libghc-primitive-dev (<< 0.8),
 libghc-primitive-prof,
 libghc-random-dev (>= 1.1),
 libghc-random-dev (<< 1.2),
 libghc-random-prof (>= 1.1),
 libghc-random-prof (<< 1.2),
 libghc-resourcet-dev (>= 1.1),
 libghc-resourcet-dev (<< 1.3),
 libghc-resourcet-prof (>= 1.1),
 libghc-resourcet-prof (<< 1.3),
 libghc-semigroups-dev (>= 0.16),
 libghc-semigroups-dev (<< 0.20),
 libghc-semigroups-prof (>= 0.16),
 libghc-transformers-base-dev (>= 0.4),
 libghc-transformers-base-dev (<< 0.5),
 libghc-transformers-base-prof (>= 0.4),
 libghc-transformers-base-prof (<< 0.5),
 libghc-wl-pprint-annotated-dev (>= 0.0),
 libghc-wl-pprint-annotated-dev (<< 0.2),
 libghc-wl-pprint-annotated-prof (>= 0.0),
 libghc-wl-pprint-annotated-prof (<< 0.2),
Build-Depends-Indep: ghc-doc,
 libghc-ansi-terminal-doc,
 libghc-async-doc,
 libghc-concurrent-output-doc,
 libghc-erf-doc,
 libghc-exceptions-doc,
 libghc-lifted-async-doc,
 libghc-mmorph-doc,
 libghc-monad-control-doc,
 libghc-pretty-show-doc,
 libghc-primitive-doc,
 libghc-random-doc,
 libghc-resourcet-doc,
 libghc-semigroups-doc,
 libghc-transformers-base-doc,
 libghc-wl-pprint-annotated-doc,
Standards-Version: 4.5.0
Homepage: https://hedgehog.qa
X-Description: Hedgehog will eat all your bugs
 Hedgehog is a modern property-based testing system, in the spirit of
 QuickCheck. Hedgehog uses integrated shrinking, so shrinks obey the
 invariants of generated values by construction.

Package: libghc-hedgehog-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-hedgehog-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-hedgehog-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
